import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.util.*;

public class FXShop {

    @FXML
    public TextField textField;
    @FXML
    public TextField price;
    @FXML
    public Button showBasket;
    @FXML
    private Button buyButton;


    Product product;

    List<Product> fullBasket = new ArrayList<>();


    public void addToBasket(ActionEvent actionEvent) {
        product = new Product(textField.getText(),Integer.parseInt( price.getText()));

        fullBasket.add(product );
        textField.clear();
        price.clear();
        Collections.sort( fullBasket, new ComparePrice() );
    }


    public void showBasket(ActionEvent actionEvent) {
        System.out.println(fullBasket);
    }


    public void insertText(ActionEvent actionEvent) {
//zmieniłem zdanie i nie używam tego
    }

}
