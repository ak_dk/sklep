import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class Shop extends Application {


    public static void main(String[] args) {
        launch( args );
    }


    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = createSceneGraph();
        Scene scene = new Scene( root );
        primaryStage.setScene( scene );
        primaryStage.show();
    }

    private Parent createSceneGraph() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader( );
        fxmlLoader.setLocation( getClass().getResource( "shoppingMall.fxml" ) );
        Parent root = fxmlLoader.load();
        return root;

    }



}
