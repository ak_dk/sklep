import java.util.Comparator;

public class ComparePrice implements Comparator<Product> {


    @Override
    public int compare(Product a, Product b) {
        return a.getPrice() < b.getPrice() ? -1 : a.getPrice() == b.getPrice() ? 0 : 1;
    }
}
